# Chatter

Playground project - chat application.

## Requirements:

- Maven 3.8.7+.
- Spring Boot version 2.7.9.
- Java (OpenJDK) version 19+.

>Keywords:
>- Chat
>- Java

## Author

- [Matija Čvrk](https://hr.linkedin.com/in/matija-%C4%8Dvrk-1388b3101/)
