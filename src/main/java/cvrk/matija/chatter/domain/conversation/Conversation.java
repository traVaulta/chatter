package cvrk.matija.chatter.domain.conversation;

import cvrk.matija.chatter.domain.conversation.message.Message;

import java.util.ArrayList;
import java.util.Collection;

public class Conversation {
    private final Collection<Message> data = new ArrayList<>();

    public Collection<String> getData() {
        return data.stream().map(Message::getContent).toList();
    }

    public void add(Message message) {
        this.data.add(message);
    }
}
