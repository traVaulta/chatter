package cvrk.matija.chatter.domain.conversation;

import cvrk.matija.chatter.domain.conversation.message.Message;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConversationService {
    private final List<Conversation> conversations = new ArrayList<>();

    public void addMessage(Message message) {
        getCurrentOrDefault().add(message);
    }

    public Conversation getCurrentOrDefault() {
        if (!any()) {
            this.conversations.add(new Conversation());
        }
        return this.conversations.get(0);
    }

    private boolean any() {
        return this.conversations.size() > 0;
    }
}
