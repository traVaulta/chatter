package cvrk.matija.chatter.domain.conversation.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    private String author;
    private String content;

    public static Message of(String author, String content) {
        return new Message(author, content);
    }
}
