package cvrk.matija.chatter.views.main;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.virtuallist.VirtualList;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import cvrk.matija.chatter.domain.conversation.ConversationService;
import cvrk.matija.chatter.domain.conversation.message.Message;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Chatter")
@Route(value = "")
public class MainView extends VerticalLayout {
    private final VirtualList<String> chat = new VirtualList<>();
    private final TextField messageTxt;
    private final Button sendBtn;
    private final ConversationService service;

    public MainView(@Autowired ConversationService conversationService) {
        service = conversationService;

        messageTxt = new TextField("Message");
        sendBtn = new Button("Send");

        sendBtn.addClickListener(e -> {
            service.addMessage(Message.of("Sam", messageTxt.getValue()));

            chat.setItems(service.getCurrentOrDefault().getData());
            messageTxt.clear();
        });
        sendBtn.addClickShortcut(Key.ENTER);

        setJustifyContentMode(JustifyContentMode.END);
        setHeight("100%");

        add(chat, messageTxt, sendBtn);
    }
}
